# Dream Road - My first Unity game

In order to learn the basics of Unity, I was asked to make a game.
The particularity here was that I wasn't given any particular instructions,
so I felt this was the perfect opportunity to do something I really like.
This is why I made this awesome arcade game, inspired by the game Audiosurf!

[Watch a demo](https://www.youtube.com/watch?v=mEWYMe9VFCE)

[Play the game!](http://theo.maria.emi.u-bordeaux.fr/unity-games/dream-road/)

It includes a scoring system, with combo mechanics : if you hit many capsules
in a row,you gain more points. On the contrary, if you get hit by an obstacle,
you lose some points, and you lose your combo! I made almost every asset myself,
including the music and the graphics!