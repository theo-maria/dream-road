﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public int initialLife = 5;
    private int remainingLife;

    public bool carIsBroken = false;

    private ParticleSystem brokenCarEffect;
    private Slider healthBar;
    private Camera mainCamera;

    private void Start()
    {
        brokenCarEffect = GameObject.FindGameObjectWithTag("BrokenEffect").GetComponent<ParticleSystem>();
        healthBar = GameObject.FindGameObjectWithTag("Health").GetComponent<Slider>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        remainingLife = initialLife;
    }

    private void breakCar()
    {
        carIsBroken = true;
        brokenCarEffect.Play();
        gameObject.GetComponent<ScoreManager>().losePointsBroken();
        mainCamera.GetComponent<Animator>().Play("CameraMultiShake");
        gameObject.GetComponent<LevelManager>().loseGame();
    }


    public void takeHit()
    {
        GetComponent<ScoreManager>().resetCombo();
        if (!carIsBroken)
        {
            remainingLife--;
            if (remainingLife <= 0)
            {
                breakCar();
            }
            else
            {
                mainCamera.GetComponent<Animator>().Play("CameraShake");
                gameObject.GetComponent<ScoreManager>().losePointsHit();
            }

            healthBar.GetComponent<Animator>().Play("HealthBarTakeHit");
            updateHealthBar();
        }
        
    }

    private void updateHealthBar()
    {
        if(remainingLife == 0)
        {
            healthBar.value = 1.0f;
            healthBar.GetComponent<Animator>().Play("BrokenHealthBar");
        }
        else
        {
            healthBar.value = (float)remainingLife / (float)initialLife;
            healthBar.GetComponent<Animator>().Play("Idle");
        }
    }
}
