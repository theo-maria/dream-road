﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameRestarter : MonoBehaviour
{
    private GameObject finalScoreText;
    private GameObject bestScoreText;

    void Start()
    {
        finalScoreText = GameObject.FindGameObjectWithTag("FinalScore");
        bestScoreText = GameObject.FindGameObjectWithTag("BestScore");
    }

    public void RestartGame()
    {
        gameObject.GetComponent<Animator>().Play("RestartButtonOut");
        finalScoreText.GetComponent<Animator>().Play("finalScoreOut");
        bestScoreText.GetComponent<Animator>().Play("bestScoreOut");
        StartCoroutine(WaitAndRestart());
    }

    IEnumerator WaitAndRestart()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Level1");
    }
}
