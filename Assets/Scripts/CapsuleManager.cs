﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleManager : MonoBehaviour
{
    public GameObject gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0,0,-Time.deltaTime * gameManager.GetComponent<LevelManager>().blocsSpeed);

        if (transform.position.z < -3.4)
        {
            Destroy(gameObject);
            gameManager.GetComponent<ScoreManager>().missCapsule();
        }
    }
}
