﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadMainMenu : MonoBehaviour
{
    public GameObject car;
    public GameObject healthBar;
    public GameObject gameManagerPrefab;
    public GameObject scoreText;
    public Button startButton;
    public GameObject mainTitleText;
    public GameObject mainTitleCross;
    public GameObject aGameByText;
    public GameObject musicByText;

    private GameObject gameManager;
    private AudioSource mainMenuSong;
    void Start()
    {
        car.GetComponent<Animator>().Play("idle_car_pregame");
        mainMenuSong = GameObject.FindGameObjectWithTag("MainMenuSong").GetComponent<AudioSource>();
        healthBar.SetActive(false);
        scoreText.SetActive(false);
        startButton.onClick.AddListener(BeginGame);
    }

    void Update()
    {
        
    }

    void BeginGame()
    {
        startButton.GetComponent<Animator>().Play("StartButtonOut");
        mainTitleText.GetComponent<Animator>().SetTrigger("EndMainTitle");
        mainTitleCross.GetComponent<Animator>().SetTrigger("EndMainTitle");
        aGameByText.GetComponent<Animator>().Play("CreditTextOut");
        musicByText.GetComponent<Animator>().Play("CreditTextOut");
        healthBar.SetActive(true);
        scoreText.SetActive(true);
        gameManager = Instantiate(gameManagerPrefab);
        car.GetComponent<DetectCollisions>().gameManager = gameManager;
        car.GetComponent<Animator>().Play("car_forward_to_center");
        mainMenuSong.GetComponent<Animator>().Play("DecreaseMenuSongLevel");
        StartCoroutine(WaitForGameActivation());
    }

    IEnumerator WaitForGameActivation()
    {
        yield return new WaitForSeconds(5);
        startButton.gameObject.SetActive(false);
        car.GetComponent<MoveCar>().gameIsActive = true;
        gameManager.GetComponent<LevelManager>().startGame();
    }
}
