﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
	public float levelDuration = 125.0f;
	public bool gameIsStarted = false;
    public float blocsSpeed;

    // <<begin time, blocs speed value>
    public SortedDictionary<float, float> speedValues;
    public float songBpm = 120.0f;
    public float songOffset = 0.0f;
    public float songClimaxTime = 99.0f;

    private float beginTime;
    private GameObject car;
    private GameObject scoreText;
    private GameObject healthBar;
    private GameObject mainCamera;
    private GameObject field;
    private bool gameIsLost = false;
    private GameObject finalScoreText;
    private GameObject bestScoreText;
    private GameObject restartButton;
    private AudioSource levelSong;
    private Coroutine currentUpdateBlocsRoutine;
    private float maxSpeedReached;
    private GameObject arrows;
    private GameObject arrowsTainted;
    private bool fieldLightsAnimation = false;


    void Start()
	{
		car = GameObject.FindGameObjectWithTag("Car");
        scoreText = GameObject.FindGameObjectWithTag("Score");
        healthBar = GameObject.FindGameObjectWithTag("Health");
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        field = GameObject.FindGameObjectWithTag("Field");
        finalScoreText = GameObject.FindGameObjectWithTag("FinalScore");
        bestScoreText = GameObject.FindGameObjectWithTag("BestScore");
        restartButton = GameObject.FindGameObjectWithTag("RestartButton");
        levelSong = GameObject.FindGameObjectWithTag("LevelSong").GetComponent<AudioSource>();
        arrows = GameObject.FindGameObjectWithTag("Arrows");
        arrowsTainted = GameObject.FindGameObjectWithTag("ArrowsTainted");

        speedValues = new SortedDictionary<float, float>();
        speedValues.Add(0.0f, 32.0f);
        speedValues.Add(36.0f, 35.0f);
        speedValues.Add(52.0f, 38.0f);
        speedValues.Add(68.0f, 41.0f);
        speedValues.Add(84.0f, 45.0f);
        speedValues.Add(100.0f, 55.0f);
        speedValues.Add(116.0f, 65.0f);
        speedValues.Add(124.0f, 70.0f);
    }

    void Update()
    {
        if (gameIsStarted)
        	if(Time.time - beginTime > levelDuration)
        		endGame();
        if (gameIsLost)
            slowGame();
        if (Time.time - beginTime > songClimaxTime && Time.time - beginTime < levelDuration + 4)
            fieldLightsAnimation = true;
        else
            fieldLightsAnimation = false;
    }

    IEnumerator updateBlocsSpeed()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
            foreach (KeyValuePair<float, float> entry in speedValues)
            {
                if (Time.time - beginTime > entry.Key && entry.Value > blocsSpeed)
                    blocsSpeed = entry.Value;
            }
        }
    }

    IEnumerator playSongBeat()
    {
        yield return new WaitForSeconds(songOffset);
        while (true)
        {
            yield return new WaitForSeconds(60 / songBpm);
            if (fieldLightsAnimation)
                field.GetComponent<Animator>().Play("FieldLightAnimation");
        }
    }

    public void startGame()
    {
        arrows.GetComponent<Animator>().Play("ArrowsAppearDisappear");
        arrowsTainted.GetComponent<Animator>().Play("ArrowTainted");
        levelSong.Play();
    	beginTime = Time.time;
    	gameIsStarted = true;
        currentUpdateBlocsRoutine = StartCoroutine(updateBlocsSpeed());
        StartCoroutine(playSongBeat());
    }

    public void endGame()
    {
        gameIsStarted = false;
    	gameObject.GetComponent<SpawnBlocs>().spawningEnabled = false;
    	StartCoroutine(finalEnd());
    }

    IEnumerator finalEnd()
    {
        yield return new WaitForSeconds(10);
        gameObject.GetComponent<ScoreManager>().setFinalScore();
        car.GetComponent<MoveCar>().gameIsActive = false;
        car.GetComponent<MoveCar>().forwardToEnd();
        mainCamera.GetComponent<Animator>().Play("CameraEndMovement");
        scoreText.GetComponent<Animator>().Play("ScoreTextOut");
        healthBar.GetComponent<Animator>().Play("HealthBarOut");
        StartCoroutine(displayScore());
    }

    public void loseGame()
    {
        maxSpeedReached = blocsSpeed;
        gameIsStarted = false;
        gameIsLost = true;
        car.GetComponent<MoveCar>().gameIsActive = false;
        gameObject.GetComponent<ScoreManager>().setFinalScore();
        StartCoroutine(finalLose());
    }

    IEnumerator finalLose()
    {
        yield return new WaitForSeconds(4);
        gameObject.GetComponent<SpawnBlocs>().spawningEnabled = false;
        mainCamera.GetComponent<Animator>().Play("CameraEndMovement");
        scoreText.GetComponent<Animator>().Play("ScoreTextOut");
        healthBar.GetComponent<Animator>().Play("HealthBarOut");
        StartCoroutine(displayScore());
    }

    IEnumerator displayScore()
    {
        finalScoreText.GetComponent<Text>().text = "Score :\n" + gameObject.GetComponent<ScoreManager>().finalScore;
        bestScoreText.GetComponent<Text>().text = "Best score :\n" + gameObject.GetComponent<ScoreManager>().bestScore;
        yield return new WaitForSeconds(7);
        finalScoreText.GetComponent<Animator>().Play("finalScoreIn");
        bestScoreText.GetComponent<Animator>().Play("bestScoreIn");
        restartButton.GetComponent<Animator>().Play("RestartButtonIn");
    }

    private void slowGame()
    {
        StopCoroutine(currentUpdateBlocsRoutine);
        // Slow the field around the road
        float currentFieldMultiplier = field.GetComponent<Animator>().GetFloat("SpeedMultiplier");
        float newFieldMultiplier = currentFieldMultiplier - (float)(0.00005 * (1.0 / Time.deltaTime) );
        newFieldMultiplier = Mathf.Max(newFieldMultiplier, 0);
        field.GetComponent<Animator>().SetFloat("SpeedMultiplier", newFieldMultiplier);

        // Slow the blocs (capsules and obstacles) to simulate car stopping
        float newSpeed = (float)(blocsSpeed * (1.0 - maxSpeedReached * 0.015 * Time.deltaTime));
        blocsSpeed = Mathf.Max(0, newSpeed);

        // Slow the music
        float newPitch = levelSong.GetComponent<AudioSource>().pitch * (float)(1.0 - 0.1*Time.deltaTime);
        levelSong.GetComponent<AudioSource>().pitch = newPitch;
    }
}
