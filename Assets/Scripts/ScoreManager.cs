﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public int score = 0;
    public int initialComboValue = 1;
    public int maxComboValue = 10;
    public int capsuleValue = 1;
    public int hitDamage = 20;
    public int brokenDamage = 200;
    public Text comboTextPrefab;
    public int finalScore;
    public int bestScore;

    private Text scoreText;
    private GameObject canvas;

    void Start()
    {
        if(!PlayerPrefs.HasKey("BestScore"))
            PlayerPrefs.SetInt("BestScore", 0);
        scoreText = GameObject.FindGameObjectWithTag("Score").GetComponent<Text>();
        canvas = GameObject.FindGameObjectWithTag("Canvas");
        bestScore = PlayerPrefs.GetInt("BestScore");
    }

    void Update()
    {
        scoreText.text = score.ToString();
    }

    public void addPoints()
    {
        if(!GetComponent<HealthManager>().carIsBroken)
        {
            score += capsuleValue;
            Text comboText = Instantiate(comboTextPrefab);
            comboText.transform.SetParent(canvas.transform);
            comboText.text = "+" + capsuleValue.ToString();
            comboText.color = new Color(1.0f, 0.5f - 0.05f*capsuleValue, 1.0f - 0.1f * capsuleValue);
            increaseCombo();
        }
    }

    public void losePointsHit()
    {
        losePoints(hitDamage);
    }

    public void losePointsBroken()
    {
        losePoints(brokenDamage);
    }

    private void losePoints(int value)
    {
        score = Mathf.Max(0, score - value);
        Text comboText = Instantiate(comboTextPrefab);
        comboText.transform.SetParent(canvas.transform);
        comboText.text = "-" + value.ToString();
        comboText.color = new Color(0.5f, 0.5f, 0.5f);
    }

    public void missCapsule()
    {
        resetCombo();
    }

    public void resetCombo()
    {
        capsuleValue = initialComboValue;
    }

    public void increaseCombo()
    {
        if (capsuleValue < maxComboValue)
        {
            capsuleValue++;
        }
    }

    public void setFinalScore()
    {
        finalScore = score;
        if (finalScore > bestScore)
        {
            bestScore = finalScore;
            PlayerPrefs.SetInt("BestScore", bestScore);
        }
    }
}
