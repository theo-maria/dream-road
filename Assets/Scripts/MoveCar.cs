﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCar : MonoBehaviour
{
    public Animator animator;
    public bool gameIsActive;
    private int pos = 0;

    void Start()
    {
        gameIsActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameIsActive)
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("center_right_movement")
            && !animator.GetCurrentAnimatorStateInfo(0).IsName("center_left_movement")
            && !animator.GetCurrentAnimatorStateInfo(0).IsName("right_center_movement")
            && !animator.GetCurrentAnimatorStateInfo(0).IsName("left_center_movement"))
            {
                if (Input.GetKey("right"))
                {
                    if (pos == -1)
                    {
                        animator.Play("left_center_movement");
                        pos++;
                    }

                    else if (pos == 0)
                    {
                        animator.Play("center_right_movement");
                        pos++;
                    }
                }
                else if (Input.GetKey("left"))
                {
                    if (pos == 1)
                    {
                        animator.Play("right_center_movement");
                        pos--;
                    }
                    else if (pos == 0)
                    {
                        animator.Play("center_left_movement");
                        pos--;
                    }
                }
            }
        }
    }

    public void forwardToEnd()
    {
        gameIsActive = false;
        gameObject.GetComponent<Animator>().SetBool("gameEnded", true);
    }
}
