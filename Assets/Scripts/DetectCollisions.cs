﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollisions : MonoBehaviour
{
    
    public GameObject pickupEffect;
    public GameObject hitEffect;

    public AudioSource hitAudioSource;

    public GameObject gameManager;

    void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Capsule"))
        {
            if (!gameManager.GetComponent<HealthManager>().carIsBroken)
            {
                gameManager.GetComponent<ScoreManager>().addPoints();
                GameObject effect = Instantiate(pickupEffect, other.transform.position + new Vector3(0, 0, -3f), other.gameObject.transform.rotation);
                StartCoroutine(waitAndDestroyEffect(effect));
                Destroy(other.gameObject);
            }
        }
        else if (other.CompareTag("Obstacle"))
        {
            if (!gameManager.GetComponent<HealthManager>().carIsBroken)
            {
                hitAudioSource.Play(0);
                GameObject effect = Instantiate(hitEffect, other.transform.position + new Vector3(0, 0, -2.5f), other.gameObject.transform.rotation);
                gameManager.GetComponent<HealthManager>().takeHit();
                StartCoroutine(waitAndDestroyEffect(effect));
                Destroy(other.gameObject);
            }
                
        }
    }

    IEnumerator waitAndDestroyEffect(GameObject effect)
    {
        yield return new WaitForSeconds(2);
        Destroy(effect);
    }
}
