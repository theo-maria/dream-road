﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisapear : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("combo_text_idle"))
            Destroy(gameObject);
    }
}
