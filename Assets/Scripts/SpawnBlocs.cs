﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBlocs : MonoBehaviour
{
    public Object capsulePrefab;
    public Object obstaclePrefab;
    public Object flamePrefab;
    [Range(0.1f, 5.0f)]
    public double blocsSpawnDelay = 1.0;
    public Vector3 startingPos0;
    public Vector3 startingPos1;
    public Vector3 startingPos2;

    public Vector3 flameStartingPos1;
    public Vector3 flameStartingPos2;

    public bool spawningEnabled = true;


    private double timeCounter;

    // Start is called before the first frame update
    void Start()
    {
        timeCounter = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time >= timeCounter)
        {
            if(spawningEnabled)
            {
                timeCounter += blocsSpawnDelay;
                int randomCapsulePos = Mathf.RoundToInt(Random.Range(0.0f, 2.0f));
                Vector3 chosenCapsulePos = startingPos0;
                switch (randomCapsulePos)
                {
                    case 0:
                        chosenCapsulePos = startingPos0;
                        break;
                    case 1:
                        chosenCapsulePos = startingPos1;
                        break;
                    case 2:
                        chosenCapsulePos = startingPos2;
                        break;
                }
                Instantiate(capsulePrefab, chosenCapsulePos, Quaternion.Euler(0,0,90));

                bool placedObstacle = false;
                for(int i=0; i<2 && !placedObstacle; i++)
                {
                    int randomObstaclePos = Mathf.RoundToInt(Random.Range(0.0f, 2.0f));
                    if (randomCapsulePos != randomObstaclePos)
                    {
                        placedObstacle = true;
                        Vector3 chosenObstaclePos = startingPos0;
                        switch (randomObstaclePos)
                        {
                            case 0:
                                chosenObstaclePos = startingPos0;
                                break;
                            case 1:
                                chosenObstaclePos = startingPos1;
                                break;
                            case 2:
                                chosenObstaclePos = startingPos2;
                                break;
                        }
                        Instantiate(obstaclePrefab, chosenObstaclePos, Quaternion.Euler(0, 0, 0));
                    }
                }
            }
        }
    }
}
